#!/usr/bin/python
# -*- coding: utf-8 -*-
#Система мониторинга изменений сервисов. Основана на inotify.
import sys, os, hashlib, argparse, time, datetime, socket, pyinotify

def_host="127.0.0.1:3001"
def_logdir="watcher/logs"
def_timeout=1.0
def_block_size=128
def_algorithm='md5'

requirements="Requirements: python 2.7, pyinotify"
descr="File changes monitoring script. Uses pyinotify."

class MyEventHandler(pyinotify.ProcessEvent):
    #def process_IN_ACCESS(self, event):
        #print "ACCESS event:", event.pathname

    #def process_IN_ATTRIB(self, event):
        #print "ATTRIB event:", event.pathname

    #def process_IN_CLOSE_NOWRITE(self, event):
        #print "CLOSE_NOWRITE event:", event.pathname

    def process_IN_CLOSE_WRITE(self, event):
	logging(str(datetime.datetime.now()) + " Close/writed: " + event.pathname, event)

    def process_IN_CREATE(self, event):
	logging(str(datetime.datetime.now()) + " New:          " + event.pathname, event)

    def process_IN_DELETE(self, event):
	logging(str(datetime.datetime.now()) + " Deleted:      " + event.pathname, event)

    def process_IN_MODIFY(self, event):
	logging(str(datetime.datetime.now()) + " Modified:     " + event.pathname, event)

    #def process_IN_OPEN(self, event):
        #print "OPEN event:", event.pathname

def get_mtime(fullname):
    return time.strftime("%d.%m.%Y %H:%M:%S", time.localtime(os.path.getmtime(fullname)))

def md5hash(filepath):
    md5 = hashlib.md5()
    f = open(filepath, 'rb')
    try:
        md5.update(f.read())
    finally:
        f.close()
    return md5.hexdigest()
  
def sha1hash(filepath):
    sha1 = hashlib.sha1()
    f = open(filepath, 'rb')
    try:
        sha1.update(f.read())
    finally:
        f.close()
    return sha1.hexdigest()
  
def get_hash(filepath):
    if algorithm=="md5":
      hash=md5hash(filepath)
    elif algorithm=="sha1":
      hash=sha1hash(filepath)
    else:
      print "Hash algorithm: invalid value:",algorithm,"(use default)"
      exec "hash={}hash(filepath)".format(def_algorithm)
    return hash
 
def logging(mess, ev):
	if mess!='':
	  fullname = ev.pathname
	  hash=get_hash(fullname)
	  if hash!="No_hash":
	    mess=mess+" "+hash+" ["+algorithm+"]"
	  print mess
	  log.write(mess + "\n")
	  log.flush()
	  mess="\n"+mess
	  if len(mess)>int(block_size):
	    mesarr=[mess[i:i+int(block_size)] for i in xrange(0, len(mess), int(block_size))]
	  else:
	    mesarr=[mess]
	  for message in mesarr:
	    client_socket.sendto(message, destination)


parser=argparse.ArgumentParser(description=descr, epilog=requirements)
parser.add_argument("dir", help="Monitoring paths, splitted by ':'") 
parser.add_argument("-a", '--hash-algorithm', action='store', dest='algorithm', help="Hash algorithm, '"+def_algorithm+"' by default")
parser.add_argument("-l", '--log', action='store', dest='logdir', help="Path to logfile, '"+def_logdir+"' by default")
parser.add_argument("-i", '--ip', action='store', dest='host', help="UDP-message host:port, '"+def_host+"' by default")
parser.add_argument("-b", '--block-size', action='store', dest='block_size', help="UDP-message block size (bytes), '"+str(def_block_size)+"' by default")
parser.add_argument("-t", '--timeout', action='store', dest='timeout', help="Check changes timeout (sec), '"+str(def_timeout)+"' by default")
parser.add_argument("-c", '--config', action='store', dest='config', help="Read parameters from file CONFIG, not used by default")
args=parser.parse_args()
dirs=args.dir.split(":")
config=args.config
if args.config:
  try:
    conf=open(config, "rb")
    exe=conf.read()
    conf.close()
  except:
    print "Unable to read", config
    exit(1)
  exec exe
for i in range(len(dirs)):
 if not os.path.exists(dirs[i-1]):
    print dirs[i-1], ": no such file or directory, skipped..."
    dirs.remove(dirs[i-1])    
if dirs==[]:
    print "No valid paths, exited"
    exit(1)
  
arguments=["logdir", "host", "timeout", "block_size", "algorithm"]
for arg in arguments:
   exec "if args.{0}:\n\t{0}=args.{0}\nelif not config:\n\t{0}=def_{0}".format(arg) 
  
try:
  timeout=float(timeout)
except:
  print "Timeout: invalid value:",timeout,"(use default)"
  timeout=def_timeout
try:
  block_size=int(block_size)
except:
  print "Block size: invalid value:",block_size,"(use default)"
  block_size=def_block_size
host=host.split(":")
try:
  destination=(host[0], int(host[1]))
except:
  print "Host: invalid value:",args.host,"(use default)"
  host=def_host.split(":")
  destination=(host[0], int(host[1]))
if algorithm!="md5" and algorithm!="sha1":
  print "Hash algorithm: invalid value:",algorithm,"(use default)"
  algorithm = def_algorithm
#dir=args.dir
logfile=logdir+"/watcher_log"+str(datetime.datetime.now().strftime('-%Y-%m-%d-%H:%M:%S'))+".txt"
if not os.path.exists(logdir):
    try:
    	os.makedirs(logdir)
    except OSError:
    	print "Failed to create", logdir
	exit(1)
log=open(logfile, "a")
client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
wm = pyinotify.WatchManager()
for dir in dirs:
  wm.add_watch(dir, pyinotify.ALL_EVENTS, rec=True)
# event handler
eh = MyEventHandler()
# notifier
notifier = pyinotify.Notifier(wm, eh)
print "Timeout:                   ", timeout
print "Log file:                  ", logfile
print "Monitoring paths           ", ", ".join(dirs)
print "Hash algorithm:            ", algorithm
print "Messaging host and port:   ", host[0]+":"+host[1]
print "Message block size, bytes: ", block_size
print "Started. Press Ctrl+C to exit\n"
while True:
	try:
		time.sleep(timeout)
        	if notifier.check_events():
            		notifier.read_events()
            		notifier.process_events()
    	except KeyboardInterrupt:
		notifier.stop()
		sys.stdout.write("\nClosing handles...")
		log.close()
		client_socket.close()
		sys.stdout.write("done!")
        	break
        except Exception, e:
	#	print e
		continue	  
	      
	      