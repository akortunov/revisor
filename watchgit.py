#!/usr/bin/python
# -*- coding: utf-8 -*-
#Система мониторинга изменений сервисов. Основана на git.
import sys, os, datetime, socket, argparse, time

requirements="Requirements: python 2.7, git"
descr="File changes monitoring script. Uses git."

def_repodir="./watcher/repodir"
def_host="127.0.0.1:3001"
def_logdir="./watcher/logs"
def_timeout=5.0
def_block_size=128

parser=argparse.ArgumentParser(description=descr, epilog=requirements)
parser.add_argument("-l", '--log', action='store', dest='logdir', help="Path to logfile, '"+def_logdir+"' by default")
parser.add_argument("-r", '--repodir', action='store', dest='repodir', help="Path to git repo, '"+def_repodir+"' by default")
parser.add_argument("-i", '--ip', action='store', dest='host', help="UDP-message host:port, '"+def_host+"' by default")
parser.add_argument("-b", '--block-size', action='store', dest='block_size', help="UDP-message block size (bytes), '"+str(def_block_size)+"' by default")
parser.add_argument("-t", '--timeout', action='store', dest='timeout', help="Check changes timeout (sec), '"+str(def_timeout)+"' by default")
parser.add_argument("-c", '--config', action='store', dest='config', help="Read parameters from file CONFIG, not used by default")
parser.add_argument("dir", help="Monitoring path")   
args=parser.parse_args()
config=args.config
if args.config:
  try:
    conf=open(config, "rb")
    exe=conf.read()
    conf.close()
  except:
    print "Unable to read", config
    exit(1)
  exec exe
arguments=["repodir", "logdir", "host", "timeout", "block_size"]
for arg in arguments:
   exec "if args.{0}:\n\t{0}=args.{0}\nelif not config:\n\t{0}=def_{0}".format(arg) 
try:
  timeout=float(timeout)
except:
  print "Timeout: invalid value:",timeout,"(use default)"
  timeout=def_timeout
try:
  block_size=int(block_size)
except:
  print "Block size: invalid value:",block_size,"(use default)"
  block_size=def_block_size
host=host.split(":")
try:
  destination=(host[0], int(host[1]))
except:
  print "Host: invalid value:",args.host,"(use default)"
  host=def_host.split(":")
  destination=(host[0], int(host[1]))
dir=args.dir
logfile=logdir+"/watcher_log"+str(datetime.datetime.now().strftime('-%Y-%m-%d-%H:%M:%S'))+".txt"
if not os.path.exists(logdir):
    try:
    	os.makedirs(logdir)
    except OSError:
    	print "Failed to create", logdir
	exit(1)
log=open(logfile, "a")
client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
if not os.path.exists(repodir):
    try:
    	os.makedirs(repodir)
    except OSError:
    	print "Failed to create", repodir
	exit(1)
rep2=repodir.replace(" ", "\ ")
if os.path.isdir(dir) == True:
	worktree=dir
elif os.path.isfile(dir) == True:
	if dir.find("/")!=-1:
	  worktree=dir[:dir.rindex("/")].replace(" ", "\ ")
	else:
	  worktree="."
	if worktree=='':
	  worktree="."
else:
	print dir, ": no such file or directory"
	exit(1)
text='Initial commit'
sys.stdout.write("Making initial commit...")
#print 'git --git-dir='+rep2+'/.git --work-tree='+worktree+' init'
os.system('git --git-dir='+rep2+'/.git --work-tree='+worktree+' init')
#print 'git --git-dir='+rep2+'/.git --work-tree='+worktree+' add -A'#+worktree+''
os.system('git --git-dir='+rep2+'/.git --work-tree='+worktree+' add -A')#+worktree+'')
#print 'git --git-dir='+rep2+'/.git --work-tree='+worktree+' commit -m '+'"'+text+'"'+' > /dev/null'
os.system('git --git-dir='+rep2+'/.git --work-tree='+worktree+' commit -m '+'"'+text+'"'+' > /dev/null')
sys.stdout.write("done!\n")

def main():
    print "Timeout:                   ", timeout
    print "Log file:                  ", logfile
    print "Git repository:            ", repodir
    print "Monitoring path:           ", dir
    print "Messaging host and port:   ", host[0]+":"+host[1]
    print "Message block size, bytes: ", block_size
    print "Started. Press Ctrl+C to exit\n"
    while True:
	try:
		time.sleep(timeout)
        	check_changes()
    	except KeyboardInterrupt:
		sys.stdout.write("\nClosing handles...")
		log.close()
		client_socket.close()
		sys.stdout.write("done!")
        	break
        except Exception, e:
		print e
		continue
        	
def check_changes():
    #print 'git --git-dir='+rep2+'/.git --work-tree='+worktree+' status'
    diff=os.popen('git --git-dir='+rep2+'/.git --work-tree='+worktree+' status').read()
    #print diff
    if diff.find("modified:")!=-1 or diff.find("deleted:")!=-1 or diff.find("Untracked files:")!=-1:
      logging(diff)

def logging(output):
        	#print text
        	text=''
        	lines=output.split("\n")
        	for line in lines:
		  if line.find("modified:")!=-1:
		    fname=worktree+"/"+line[14:]
		    text=text+line[2:12]+fname+" ("+time.strftime("%d.%m.%Y %H:%M:%S", time.localtime(os.path.getmtime(fname)))+")\n"
		for line in lines:
		  if line.find("deleted:")!=-1:
		    fname=worktree+"/"+line[14:]
		    text=text+line[2:11]+fname+" ("+datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S")+")\n"
		i=j=0
		for line in lines:
		  if line.find("Untracked files:")!=-1:
		    break
		  else:
		    i+=1
		for line in lines:
		  if line.find("untracked files present \(use \"git add\" to track\)")!=-1:
		    break
		  else:
		    j+=1
		lines2=lines[i+3:j-2]
		#print lines2
		for line in lines2:
		  text=text+'new: '+worktree+"/"+line[2:]+" ("+datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S")+")\n"
		if text=='':
		  text='default message'
		#print 'git --git-dir='+rep2+'/.git --work-tree='+worktree+' add -A'#+worktree+''
		os.system('git --git-dir='+rep2+'/.git --work-tree='+worktree+' add -A')#+worktree+'')
		#print 'git --git-dir='+rep2+'/.git --work-tree='+worktree+' commit -m '+'"'+text+'"'#+' > /dev/null'
		os.system('git --git-dir='+rep2+'/.git --work-tree='+worktree+' commit -m '+'"'+text+'"')#+' > /dev/null')
		#print 'git --git-dir='+rep2+'/.git --work-tree='+worktree+' diff master~1 '#-- '+dest
		#mess=os.popen('git --git-dir='+rep2+'/.git --work-tree='+worktree+' diff -a -D master~1' ).read() #???
		mess=os.popen('git --git-dir='+rep2+'/.git --work-tree='+worktree+' diff --diff-filter=CMRTUXB -D master~1' ).read()
		print mess
		log.write(text + "\n"+ mess + "\n")
		log.flush()
		mess="\n"+mess
		if len(mess)>int(block_size):
		  mesarr=[mess[i:i+int(block_size)] for i in xrange(0, len(mess), int(block_size))]
		else:
		  mesarr=[mess]
		for message in mesarr:
		  client_socket.sendto(message, destination)

if __name__ == '__main__':
    main()