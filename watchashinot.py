#!/usr/bin/python
# -*- coding: utf-8 -*-
#Система мониторинга изменений сервисов. Основана на inotify + sqlite3.
import sys, os, hashlib, argparse, sqlite3, time, datetime, socket, pyinotify

def_dbase = 'watcher/Hashes.db'
def_host="127.0.0.1:3001"
def_logdir="watcher/logs"
def_timeout=1.0
def_block_size=128
def_algorithm='md5'

requirements="Requirements: python 2.7, pyinotify"
descr="File changes monitoring script. Uses pyinotify and sqlite3."

CREATE_BASE = """CREATE TABLE files(fullname TEXT, hash TEXT, mtime TEXT, type TEXT)"""
ADD_NEW_FILE = """INSERT INTO files(fullname, hash, mtime, type) VALUES("{0}", "{1}", "{2}", "{3}")"""
FIND_FILE = """SELECT * FROM files WHERE fullname='{}'"""
DELETE_RECORD = """DELETE * FROM files WHERE fullname='{}'"""
UPDATE_RECORD = """UPDATE files SET hash='{1}', mtime='{2}', type='{3}' WHERE fullname='{0}'"""

dbase=''

class MyEventHandler(pyinotify.ProcessEvent):
    #def process_IN_ACCESS(self, event):
        #print "ACCESS event:", event.pathname

    #def process_IN_ATTRIB(self, event):
        #print "ATTRIB event:", event.pathname

    #def process_IN_CLOSE_NOWRITE(self, event):
        #print "CLOSE_NOWRITE event:", event.pathname

    def process_IN_CLOSE_WRITE(self, event):
	logging("CLOSE_WRITE", event)

    def process_IN_CREATE(self, event):
	logging("CREATE", event)

    def process_IN_DELETE(self, event):
	logging("DELETE", event)

    def process_IN_MODIFY(self, event):
	logging("MODIFY", event)

    #def process_IN_OPEN(self, event):
        #print "OPEN event:", event.pathname

def get_mtime(fullname):
    return time.strftime("%d.%m.%Y %H:%M:%S", time.localtime(os.path.getmtime(fullname)))

def connectDB(dbase):
    conn = sqlite3.connect(dbase)
    cursor = conn.cursor()
    return conn, cursor
  
def AddNewFile(dbase, fullname, hash, algorithm):
    conn, cursor = connectDB(dbase)
    cursor.execute(ADD_NEW_FILE.format(fullname, hash, get_mtime(fullname.replace('\one_quote', "'")), algorithm))
    conn.commit()
    conn.close()
    
def UpdateRecord(dbase, fullname, hash, algorithm):
    conn, cursor = connectDB(dbase)
    cursor.execute(UPDATE_RECORD.format(fullname, hash, get_mtime(fullname.replace('\one_quote', "'")), algorithm))
    conn.commit()
    conn.close()
    
def FindFile(dbase, fullname):
    fullname=fullname.replace("'", '\one_quote')
    #print fullname
    conn, cursor = connectDB(dbase)
    #print FIND_FILE.format(fullname)
    cursor.execute(FIND_FILE.format(fullname))
    data = cursor.fetchall()
    #conn.close()
    return data

def DeleteRecord(dbase, fullname):
    conn, cursor = connectDB(dbase)
    cursor.execute(DELETE_RECORD.format(fullname))
    conn.commit()
    conn.close()

def initDBASE(dbase):
    if dbase.find("/")!=-1:
      basepath=dbase[:dbase.rindex("/")]
    else:
      basepath=""
    if not os.path.exists(dbase):
        try:
	    if basepath!="" and not os.path.exists(basepath):
	      os.makedirs(basepath)
	    #conn=null
            print "Creating database '{}'...".format(dbase)
            conn, cursor = connectDB(dbase)
            cursor.execute(CREATE_BASE)
            conn.close()
            print "done!"
        except Exception, e:
            print "failed!"
            print e
            exit(1)
  
def md5hash(filepath):
    md5 = hashlib.md5()
    f = open(filepath, 'rb')
    try:
        md5.update(f.read())
    finally:
        f.close()
    return md5.hexdigest()
  
def sha1hash(filepath):
    sha1 = hashlib.sha1()
    f = open(filepath, 'rb')
    try:
        sha1.update(f.read())
    finally:
        f.close()
    return sha1.hexdigest()
  
def get_hash(filepath):
    if algorithm=="md5":
      hash=md5hash(filepath)
    elif algorithm=="sha1":
      hash=sha1hash(filepath)
    else:
      print "Hash algorithm: invalid value:",algorithm,"(use default)"
      exec "hash={}hash(filepath)".format(def_algorithm)
    return hash

def init_base(directories):
  #sys.stdout.write("Filling database '{}'...".format(dbase))
  print "Filling database '{}'...".format(dbase)
  for directory in directories:
    for root, dirs, files in os.walk(directory):
      for name in files:
        fullname = os.path.join(root, name)
        #if fullname.find("'")!=-1:
	#  continue
        record=FindFile(dbase, fullname)
        if not os.path.exists(fullname):
	  #DeleteRecord(dbase, fullname)
	  continue
        hash=get_hash(fullname)
        if hash=="No_hash":
	  #return
	  continue
        if record==[]:
	  AddNewFile(dbase, fullname.replace("'", '\one_quote'), hash, algorithm)
	  #mess="New: {0} ({1}) {2}".format(fullname, get_mtime(fullname), hash)
	else:
	  if hash!=record[0][1]:
	     UpdateRecord(dbase, fullname.replace("'", '\one_quote'), hash, algorithm)
	     #mess="Modified: {0} ({1}) {2} -> {3}".format(fullname, get_mtime(fullname), record[0][1], hash)
  print "done!"
  
def sendlog(mess):
	if mess!='':
	  print mess
	  log.write(mess + "\n")
	  log.flush()
	  mess="\n"+mess
	  if len(mess)>int(block_size):
	    mesarr=[mess[i:i+int(block_size)] for i in xrange(0, len(mess), int(block_size))]
	  else:
	    mesarr=[mess]
	  for message in mesarr:
	    client_socket.sendto(message, destination)
  
def logging(etype, ev):
        mess=''
        fullname = ev.pathname
        record=FindFile(dbase, fullname)
        if etype=="DELETE":
	  #print "DELETED"
	  mess="Deleted: {0}".format(fullname)
	  sendlog(mess)
	  #print mess
	  DeleteRecord(dbase, fullname)
	  #continue
	  #return
	else:
	  hash=get_hash(fullname)
	  if hash=="No_hash":
	    return
	    #continue
	  if record==[]:
	    AddNewFile(dbase, fullname.replace("'", '\one_quote'), hash, algorithm)
	    mess="New: {0} ({1}) {2} [{3}]".format(fullname, get_mtime(fullname), hash, algorithm)
	  else:
	    if hash!=record[0][1]:
	      UpdateRecord(dbase, fullname.replace("'", '\one_quote'), hash, algorithm)
	      mess="Modified: {0} ({1}) {2} [{4}] -> {3} [{5}]".format(fullname, get_mtime(fullname), record[0][1], hash, record[0][3], algorithm)
	#print mess
	  sendlog(mess)



parser=argparse.ArgumentParser(description=descr, epilog=requirements)
parser.add_argument("dir", help="Monitoring paths, splitted by ':'")   
parser.add_argument("-a", '--hash-algorithm', action='store', dest='algorithm', help="Hash algorithm, '"+def_algorithm+"' by default")
parser.add_argument("-l", '--log', action='store', dest='logdir', help="Path to logfile, '"+def_logdir+"' by default")
parser.add_argument("-d", '--database', action='store', dest='dbase', help="Database name, '"+def_dbase+"' by default")
parser.add_argument("-i", '--ip', action='store', dest='host', help="UDP-message host:port, '"+def_host+"' by default")
parser.add_argument("-b", '--block-size', action='store', dest='block_size', help="UDP-message block size (bytes), '"+str(def_block_size)+"' by default")
parser.add_argument("-t", '--timeout', action='store', dest='timeout', help="Check changes timeout (sec), '"+str(def_timeout)+"' by default")
parser.add_argument("-c", '--config', action='store', dest='config', help="Read parameters from file CONFIG, not used by default")
parser.add_argument("-f", '--fast', action='store_true', dest='fast', help="Fast start (without filling database)")
args=parser.parse_args()
dirs=args.dir.split(":")
config=args.config
if args.config:
  try:
    conf=open(config, "rb")
    exe=conf.read()
    conf.close()
  except:
    print "Unable to read", config
    exit(1)
  exec exe
for i in range(len(dirs)):
 if not os.path.exists(dirs[i-1]):
    print dirs[i-1], ": no such file or directory, skipped..."
    dirs.remove(dirs[i-1])    
if dirs==[]:
    print "No valid paths, exited"
    exit(1)
  
arguments=["dbase", "logdir", "host", "timeout", "block_size", "algorithm"]
for arg in arguments:
   exec "if args.{0}:\n\t{0}=args.{0}\nelif not config:\n\t{0}=def_{0}".format(arg) 
initDBASE(dbase)
if not args.fast:
  init_base(dirs)
  
try:
  timeout=float(timeout)
except:
  print "Timeout: invalid value:",timeout,"(use default)"
  timeout=def_timeout
try:
  block_size=int(block_size)
except:
  print "Block size: invalid value:",block_size,"(use default)"
  block_size=def_block_size
host=host.split(":")
try:
  destination=(host[0], int(host[1]))
except:
  print "Host: invalid value:",args.host,"(use default)"
  host=def_host.split(":")
  destination=(host[0], int(host[1]))
if algorithm!="md5" and algorithm!="sha1":
  print "Hash algorithm: invalid value:",algorithm,"(use default)"
  algorithm = def_algorithm
dir=args.dir
logfile=logdir+"/watcher_log"+str(datetime.datetime.now().strftime('-%Y-%m-%d-%H:%M:%S'))+".txt"
if not os.path.exists(logdir):
    try:
    	os.makedirs(logdir)
    except OSError:
    	print "Failed to create", logdir
	exit(1)
log=open(logfile, "a")
client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
wm = pyinotify.WatchManager()
for dir in dirs:
  wm.add_watch(dir, pyinotify.ALL_EVENTS, rec=True)
# event handler
eh = MyEventHandler()
# notifier
notifier = pyinotify.Notifier(wm, eh)
print "Timeout:                   ", timeout
print "Log file:                  ", logfile
print "Database:                  ", dbase
print "Monitoring path:           ", ", ".join(dirs)
print "Hash algorithm:            ", algorithm
print "Messaging host and port:   ", host[0]+":"+host[1]
print "Message block size, bytes: ", block_size
print "Started. Press Ctrl+C to exit\n"
while True:
	try:
		time.sleep(timeout)
        	#check_changes()
        	if notifier.check_events():
            		notifier.read_events()
            		notifier.process_events()
    	except KeyboardInterrupt:
		notifier.stop()
		sys.stdout.write("\nClosing handles...")
		log.close()
		client_socket.close()
		sys.stdout.write("done!")
        	break
        except Exception, e:
	#	print e
		continue	  
	      
	      